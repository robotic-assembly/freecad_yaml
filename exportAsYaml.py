# Import the necessary modules
import FreeCAD
import Part
import yaml

# Get the active document
doc = FreeCAD.open("./bridge.fcstd")

# Get all the objects in the document
objects = doc.Objects

# Create empty lists to store parts
parts = []

# Loop through all the objects in the document
for obj in objects:
    # Check if the object is a part
    if isinstance(obj, Part.Feature):
        # Check if the label contains "Cube"
        if "Cube" in obj.Label:
            # Add the object to the parts list     
            parts.append(obj)

# Create an empty dictionary to store the parts' information
parts_info = []

# Loop through all the parts in the parts list
for part in parts:
    # Get the part's label, size, and position
    label = part.Label  
    length, width, height = part.Shape.BoundBox.XLength, part.Shape.BoundBox.YLength, part.Shape.BoundBox.ZLength
    x, y, z = part.Placement.Base.x, part.Placement.Base.y, part.Placement.Base.z
    size = {"length": part.Shape.BoundBox.XLength, "width": part.Shape.BoundBox.YLength, "height": part.Shape.BoundBox.ZLength}
    position = {"x": part.Placement.Base.x, "y": part.Placement.Base.y, "z": part.Placement.Base.z}


    # Add the part's information to the list
    parts_info.append({"name": label, "size": size, "position": position})


print(parts_info)

# Write the parts' information to a Yaml file
with open("./parts_info.yaml", "w") as file:
    yaml.dump(parts_info, file)




